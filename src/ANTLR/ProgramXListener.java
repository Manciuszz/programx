// Generated from src/Gramatika/ProgramX.g4 by ANTLR 4.7
package ANTLR;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ProgramXParser}.
 */
public interface ProgramXListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(ProgramXParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(ProgramXParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(ProgramXParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(ProgramXParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(ProgramXParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(ProgramXParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(ProgramXParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(ProgramXParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierFunctionCall(ProgramXParser.IdentifierFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierFunctionCall(ProgramXParser.IdentifierFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printlnFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterPrintlnFunctionCall(ProgramXParser.PrintlnFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printlnFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitPrintlnFunctionCall(ProgramXParser.PrintlnFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterPrintFunctionCall(ProgramXParser.PrintFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitPrintFunctionCall(ProgramXParser.PrintFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code logFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterLogFunctionCall(ProgramXParser.LogFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code logFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitLogFunctionCall(ProgramXParser.LogFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assertFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterAssertFunctionCall(ProgramXParser.AssertFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assertFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitAssertFunctionCall(ProgramXParser.AssertFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code sizeFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterSizeFunctionCall(ProgramXParser.SizeFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code sizeFunctionCall}
	 * labeled alternative in {@link ProgramXParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitSizeFunctionCall(ProgramXParser.SizeFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(ProgramXParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(ProgramXParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#ifStat}.
	 * @param ctx the parse tree
	 */
	void enterIfStat(ProgramXParser.IfStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#ifStat}.
	 * @param ctx the parse tree
	 */
	void exitIfStat(ProgramXParser.IfStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#elseIfStat}.
	 * @param ctx the parse tree
	 */
	void enterElseIfStat(ProgramXParser.ElseIfStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#elseIfStat}.
	 * @param ctx the parse tree
	 */
	void exitElseIfStat(ProgramXParser.ElseIfStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#elseStat}.
	 * @param ctx the parse tree
	 */
	void enterElseStat(ProgramXParser.ElseStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#elseStat}.
	 * @param ctx the parse tree
	 */
	void exitElseStat(ProgramXParser.ElseStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDecl(ProgramXParser.FunctionDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDecl(ProgramXParser.FunctionDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(ProgramXParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(ProgramXParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#idList}.
	 * @param ctx the parse tree
	 */
	void enterIdList(ProgramXParser.IdListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#idList}.
	 * @param ctx the parse tree
	 */
	void exitIdList(ProgramXParser.IdListContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#exprList}.
	 * @param ctx the parse tree
	 */
	void enterExprList(ProgramXParser.ExprListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#exprList}.
	 * @param ctx the parse tree
	 */
	void exitExprList(ProgramXParser.ExprListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGtExpression(ProgramXParser.GtExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGtExpression(ProgramXParser.GtExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNumberExpression(ProgramXParser.NumberExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNumberExpression(ProgramXParser.NumberExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierExpression(ProgramXParser.IdentifierExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierExpression(ProgramXParser.IdentifierExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterModulusExpression(ProgramXParser.ModulusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitModulusExpression(ProgramXParser.ModulusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotExpression(ProgramXParser.NotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotExpression(ProgramXParser.NotExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplyExpression(ProgramXParser.MultiplyExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplyExpression(ProgramXParser.MultiplyExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGtEqExpression(ProgramXParser.GtEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGtEqExpression(ProgramXParser.GtEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(ProgramXParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(ProgramXParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterStringExpression(ProgramXParser.StringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitStringExpression(ProgramXParser.StringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expressionExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpressionExpression(ProgramXParser.ExpressionExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expressionExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpressionExpression(ProgramXParser.ExpressionExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNullExpression(ProgramXParser.NullExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNullExpression(ProgramXParser.NullExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionCallExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCallExpression(ProgramXParser.FunctionCallExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionCallExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCallExpression(ProgramXParser.FunctionCallExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code listExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterListExpression(ProgramXParser.ListExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code listExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitListExpression(ProgramXParser.ListExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtEqExpression(ProgramXParser.LtEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtEqExpression(ProgramXParser.LtEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtExpression(ProgramXParser.LtExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtExpression(ProgramXParser.LtExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpression(ProgramXParser.BoolExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpression(ProgramXParser.BoolExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotEqExpression(ProgramXParser.NotEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotEqExpression(ProgramXParser.NotEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDivideExpression(ProgramXParser.DivideExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDivideExpression(ProgramXParser.DivideExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpression(ProgramXParser.OrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpression(ProgramXParser.OrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryMinusExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryMinusExpression(ProgramXParser.UnaryMinusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryMinusExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryMinusExpression(ProgramXParser.UnaryMinusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code powerExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPowerExpression(ProgramXParser.PowerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code powerExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPowerExpression(ProgramXParser.PowerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqExpression(ProgramXParser.EqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqExpression(ProgramXParser.EqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInExpression(ProgramXParser.InExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInExpression(ProgramXParser.InExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddExpression(ProgramXParser.AddExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddExpression(ProgramXParser.AddExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubtractExpression(ProgramXParser.SubtractExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubtractExpression(ProgramXParser.SubtractExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTernaryExpression(ProgramXParser.TernaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTernaryExpression(ProgramXParser.TernaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inputExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInputExpression(ProgramXParser.InputExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inputExpression}
	 * labeled alternative in {@link ProgramXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInputExpression(ProgramXParser.InputExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#list}.
	 * @param ctx the parse tree
	 */
	void enterList(ProgramXParser.ListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#list}.
	 * @param ctx the parse tree
	 */
	void exitList(ProgramXParser.ListContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProgramXParser#indexes}.
	 * @param ctx the parse tree
	 */
	void enterIndexes(ProgramXParser.IndexesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProgramXParser#indexes}.
	 * @param ctx the parse tree
	 */
	void exitIndexes(ProgramXParser.IndexesContext ctx);
}