grammar ProgramX;

parse
 : block EOF
 ;

block
 : (statement | functionDecl)* (Return expression ';')?
 ;

statement
 : assignment ';'
 | functionCall ';'
 | ifStatement
 | forStatement
 ;

assignment
 : Identifier indexes? '=' expression
 ;

functionCall
 : Identifier '(' exprList? ')'      #identifierFunctionCall
 | SpausdinkEil '(' expression? ')'  #printlnFunctionCall
 | Spausdink '(' expression ')'      #printFunctionCall
 | SpausdinkFaile '(' exprList? ')'  #logFunctionCall
 | Assert '(' expression ')'         #assertFunctionCall
 | Size '(' expression ')'           #sizeFunctionCall
 ;

ifStatement
 : ifStat elseIfStat* elseStat? End
 ;

ifStat
 : If expression Tada block
 ;

elseIfStat
 : Arba If expression Tada block
 ;

elseStat
 : Arba Tada block
 ;

functionDecl
 : FuncDecl Identifier '(' idList? ')' block End
 ;

forStatement
 : Roll Identifier '=' expression Iki expression Tada block End
 ;

idList
 : Identifier (',' Identifier)*
 ;

exprList
 : expression (',' expression)*
 ;

expression
 : '-' expression                           #unaryMinusExpression
 | '!' expression                           #notExpression
 | expression '^' expression                #powerExpression
 | expression '*' expression                #multiplyExpression
 | expression '/' expression                #divideExpression
 | expression '%' expression                #modulusExpression
 | expression '+' expression                #addExpression
 | expression '-' expression                #subtractExpression
 | expression '>=' expression               #gtEqExpression
 | expression '<=' expression               #ltEqExpression
 | expression '>' expression                #gtExpression
 | expression '<' expression                #ltExpression
 | expression '==' expression               #eqExpression
 | expression '!=' expression               #notEqExpression
 | expression '&&' expression               #andExpression
 | expression '||' expression               #orExpression
 | expression '?' expression ':' expression #ternaryExpression
 | expression In expression                 #inExpression
 | Number                                   #numberExpression
 | Bool                                     #boolExpression
 | Null                                     #nullExpression
 | functionCall indexes?                    #functionCallExpression
 | list indexes?                            #listExpression
 | Identifier indexes?                      #identifierExpression
 | Line indexes?                            #stringExpression
 | '(' expression ')' indexes?              #expressionExpression
 | Input '(' Line? ')'                      #inputExpression
 ;

list
 : '[' exprList? ']'
 ;

indexes
 : ('[' expression ']')+
 ;

SpausdinkEil   : 'spausdinkEil';
Spausdink      : 'spausdink';
SpausdinkFaile : 'spausdinkFaile';
Input    : 'input';
Assert   : 'assert';
Size     : 'size';
FuncDecl : 'InitializeF';
If       : 'jei';
Arba     : 'arba';
Return   : 'return';
Roll     : 'roll';
Iki      : 'iki';
Tada     : 'tada';
End      : 'px';
In       : 'in';
Null     : 'null';

Or       : '||';
And      : '&&';
Equals   : '==';
NEquals  : '!=';
GTEquals : '>=';
LTEquals : '<=';
Pow      : '^';
Excl     : '!';
GT       : '>';
LT       : '<';
Add      : '+';
Subtract : '-';
Multiply : '*';
Divide   : '/';
Modulus  : '%';
OBrace   : '{';
CBrace   : '}';
OBracket : '[';
CBracket : ']';
OParen   : '(';
CParen   : ')';
SColon   : ';';
Assign   : '=';
Comma    : ',';
QMark    : '?';
Colon    : ':';

Bool
 : 'true' 
 | 'false'
 ;

Number
 : Int ('.' Digit*)?
 ;

Identifier
 : [a-zA-Z_] [a-zA-Z_0-9]*
 ;

Line
 : ["] (~["\r\n] | '\\\\' | '\\"')* ["]
 | ['] (~['\r\n] | '\\\\' | '\\\'')* [']
 ;

Comment
 : ('Komentaras/' ~[\r\n]* | '/*' .*? '*/') -> skip
 ;

Space
 : [ \t\r\n\u000C] -> skip
 ;

fragment Int
 : [1-9] Digit*
 | '0'
 ;
  
fragment Digit 
 : [0-9]
 ;