package ProgramX;

import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
       
import ANTLR.*;
import java.io.IOException;
import org.antlr.v4.runtime.RecognitionException;

public class Main {
    public static void main(String[] args) {
        try {
            if (args.length != 0) {
                ProgramXLexer lexer = new ProgramXLexer(new ANTLRFileStream(args[0]));
                ProgramXParser parser = new ProgramXParser(new CommonTokenStream(lexer));
                parser.setBuildParseTree(true);
                ParseTree tree = parser.parse();

                Scope scope = new Scope();
                Map<String, Function> functions = new HashMap<>();
                FuncParamVisitor symbolVisitor = new FuncParamVisitor(functions);
                symbolVisitor.visit(tree);
                ASTWalker visitor = new ASTWalker(scope, functions);
                visitor.visit(tree);
            } else {
                System.out.println("Error: No test file found");
            }
        } catch (IOException | RecognitionException e) {
            if ( e.getMessage() != null) {
                System.err.println(e.getMessage());
            } else {
                e.printStackTrace();
            }
        }
    }
}
