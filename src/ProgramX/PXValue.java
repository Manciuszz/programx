package ProgramX;

import java.util.Arrays;
import java.util.List;

public class PXValue implements Comparable<PXValue> {

    public static final PXValue NULL = new PXValue();
    public static final PXValue VOID = new PXValue();

    private Object value;

    private PXValue() {
        // private constructor: only used for NULL and VOID
        value = new Object();
    }

    public PXValue(Object v) {
        if(v == null) {
            throw new RuntimeException("v == null");
        }
        value = v;
        // only accept boolean, list, number or string types
        if(!(isBoolean() || isList() || is2DList() || isNumber() || isString())) {
            throw new RuntimeException("invalid data type: " + v + " (" + v.getClass() + ")");
        }
    }

    public int asNumber() {
        return ((Number)value).intValue();
    }
    
    public Boolean asBoolean() {
        return (Boolean)value;
    }

    public Double asDouble() {
        return ((Number)value).doubleValue();
    }

    public Long asLong() {
        return ((Number)value).longValue();
    }

    @SuppressWarnings("unchecked")
    public List<PXValue> asList() {
        return (List<PXValue>)value;
    }  
    
    public int[][] asIntList() {
        List<PXValue> ls = ((List<PXValue>)value);
        int[][] arr = new int[ls.size()][];
        for (int i = 0; i < ls.size(); i++) {
            int[] row = ls.get(i).toArray();
            arr[i] = new int[row.length];
            for(int j = 0; j < row.length; j++ ) {          
                arr[i][j] = row[j];
            }
        }
        return arr;
    }  

    public String asString() {
        return (String)value;
    }

    //@Override
    public int compareTo(PXValue that) {
        if(this.isNumber() && that.isNumber()) {
            if(this.equals(that)) {
                return 0;
            }
            else {
                return this.asDouble().compareTo(that.asDouble());
            }
        }
        else if(this.isString() && that.isString()) {
            return this.asString().compareTo(that.asString());
        }
        else {
            throw new RuntimeException("illegal expression: can't compare `" + this + "` to `" + that + "`");
        }
    }

    @Override
    public boolean equals(Object o) {
        if(this == VOID || o == VOID) {
            throw new RuntimeException("can't use VOID: " + this + " ==/!= " + o);
        }
        if(this == o) {
            return true;
        }
        if(o == null || this.getClass() != o.getClass()) {
            return false;
        }
        PXValue that = (PXValue)o;
        if(this.isNumber() && that.isNumber()) {
            double diff = Math.abs(this.asDouble() - that.asDouble());
            return diff < 0.00000000001;
        }
        else {
            return this.value.equals(that.value);
        }
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    public boolean isBoolean() {
        return value instanceof Boolean;
    }

    public boolean isNumber() {
        return value instanceof Number;
    }

    public boolean isList() {
        return value instanceof List<?>;
    }
    
    public boolean is2DList() {
        return value instanceof int[][];
    }
    
    public boolean isNull() {
        return this == NULL;
    }

    public boolean isVoid() {
        return this == VOID;
    }

    public boolean isString() {
        return value instanceof String;
    }

    private String printMatrix() {
        String txt = "";
        int[][] c = (int[][])value;
        for(int i = 0; i < c.length; i++) {
            txt += Arrays.toString(c[i]);
            if (i != c.length - 1) 
                txt += ", ";
        }
        return txt;
    }
    
    @Override
    public String toString() {
        return isNull() ? "NULL" : isVoid() ? "VOID" : is2DList() ? printMatrix() : String.valueOf(value);
    }
    
    public int[] toArray() {
        int i=0;
        int[] array = new int[this.size()];
        for(PXValue e : this.asList())
            array[i++] = e.asNumber();  
        return array;
    }
        
    public Integer size() {
        return ((List)value).size();
    }
}
