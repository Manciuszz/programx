package ProgramX;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;

import ANTLR.*;
import ANTLR.ProgramXParser.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

class EvalException extends RuntimeException {

    public EvalException(ParserRuleContext ctx) {
        this("Blogas expression: " + ctx.getText(), ctx);
    }

    public EvalException(String msg, ParserRuleContext ctx) {
        super(msg + " eilute:" + ctx.start.getLine());
    }
}

public class ASTWalker extends ProgramXBaseVisitor<PXValue> {

    private static RValue returnValue = new RValue();
    private Scope scope;
    private Map<String, Function> functions;

    public ASTWalker(Scope scope, Map<String, Function> functions) {
        this.scope = scope;
        this.functions = functions;
    }

    @Override
    public PXValue visitFunctionDecl(FunctionDeclContext ctx) {
        return PXValue.VOID;
    }

    @Override
    public PXValue visitList(ListContext ctx) {
        List<PXValue> list = new ArrayList<PXValue>();
        if (ctx.exprList() != null) {
            for (ExpressionContext ex : ctx.exprList().expression()) {
                list.add(this.visit(ex));
            }
        }
        return new PXValue(list);
    }

    @Override
    public PXValue visitUnaryMinusExpression(UnaryMinusExpressionContext ctx) {
        PXValue v = this.visit(ctx.expression());
        if (!v.isNumber()) {
            throw new EvalException(ctx);
        }
        return new PXValue(-1 * v.asDouble());
    }

    @Override
    public PXValue visitNotExpression(NotExpressionContext ctx) {
        PXValue v = this.visit(ctx.expression());
        if (!v.isBoolean()) {
            throw new EvalException(ctx);
        }
        return new PXValue(!v.asBoolean());
    }

    @Override
    public PXValue visitPowerExpression(PowerExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(Math.pow(lhs.asDouble(), rhs.asDouble()));
        }
        throw new EvalException(ctx);
    }

    public static int[][] multiply(int[][] a, int[][] b) {
        int rowsInA = a.length;
        int columnsInA = a[0].length; // same as rows in B
        int columnsInB = b[0].length;
        int[][] c = new int[rowsInA][columnsInB];
        for (int i = 0; i < rowsInA; i++) {
            for (int j = 0; j < columnsInB; j++) {
                for (int k = 0; k < columnsInA; k++) {
                    c[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return c;
    }
    
    @Override
    public PXValue visitMultiplyExpression(MultiplyExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs == null || rhs == null) {
            System.err.println("lhs " + lhs + " rhs " + rhs);
            throw new EvalException(ctx);
        }

        // number * number
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() * rhs.asDouble());
        }

        // string * number
        if (lhs.isString() && rhs.isNumber()) {
            StringBuilder str = new StringBuilder();
            int stop = rhs.asDouble().intValue();
            for (int i = 0; i < stop; i++) {
                str.append(lhs.asString());
            }
            return new PXValue(str.toString());
        }

        // list * number
        if (lhs.isList() && rhs.isNumber()) {
            List<PXValue> total = new ArrayList<>();
            int stop = rhs.asDouble().intValue();
            for (int i = 0; i < stop; i++) {
                total.addAll(lhs.asList());
            }
            return new PXValue(total);
        }
        
        // list[lists] * list[lists] = Matrix Multiplication
        if (lhs.isList() && rhs.isList()) {
            int[][] c = multiply(lhs.asIntList(), rhs.asIntList());
            return new PXValue(c);
        }
        
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitDivideExpression(DivideExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() / rhs.asDouble());
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitModulusExpression(ModulusExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() % rhs.asDouble());
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitAddExpression(@NotNull ProgramXParser.AddExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));

        if (lhs == null || rhs == null) {
            throw new EvalException(ctx);
        }

        // number + number
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() + rhs.asDouble());
        }

        // list + any
        if (lhs.isList()) {
            List<PXValue> list = lhs.asList();
            list.add(rhs);
            return new PXValue(list);
        }

        // string + any
        if (lhs.isString()) {
            return new PXValue(lhs.asString() + "" + rhs.toString());
        }

        // any + string
        if (rhs.isString()) {
            return new PXValue(lhs.toString() + "" + rhs.asString());
        }

        return new PXValue(lhs.toString() + rhs.toString());
    }

    @Override
    public PXValue visitSubtractExpression(SubtractExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() - rhs.asDouble());
        }
        if (lhs.isList()) {
            List<PXValue> list = lhs.asList();
            list.remove(rhs);
            return new PXValue(list);
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitGtEqExpression(GtEqExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() >= rhs.asDouble());
        }
        if (lhs.isString() && rhs.isString()) {
            return new PXValue(lhs.asString().compareTo(rhs.asString()) >= 0);
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitLtEqExpression(LtEqExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() <= rhs.asDouble());
        }
        if (lhs.isString() && rhs.isString()) {
            return new PXValue(lhs.asString().compareTo(rhs.asString()) <= 0);
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitGtExpression(GtExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() > rhs.asDouble());
        }
        if (lhs.isString() && rhs.isString()) {
            return new PXValue(lhs.asString().compareTo(rhs.asString()) > 0);
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitLtExpression(LtExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs.isNumber() && rhs.isNumber()) {
            return new PXValue(lhs.asDouble() < rhs.asDouble());
        }
        if (lhs.isString() && rhs.isString()) {
            return new PXValue(lhs.asString().compareTo(rhs.asString()) < 0);
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitEqExpression(@NotNull ProgramXParser.EqExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        if (lhs == null) {
            throw new EvalException(ctx);
        }
        return new PXValue(lhs.equals(rhs));
    }

    @Override
    public PXValue visitNotEqExpression(@NotNull ProgramXParser.NotEqExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));
        return new PXValue(!lhs.equals(rhs));
    }

    @Override
    public PXValue visitAndExpression(AndExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));

        if (!lhs.isBoolean() || !rhs.isBoolean()) {
            throw new EvalException(ctx);
        }
        return new PXValue(lhs.asBoolean() && rhs.asBoolean());
    }

    @Override
    public PXValue visitOrExpression(OrExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));

        if (!lhs.isBoolean() || !rhs.isBoolean()) {
            throw new EvalException(ctx);
        }
        return new PXValue(lhs.asBoolean() || rhs.asBoolean());
    }

    @Override
    public PXValue visitTernaryExpression(TernaryExpressionContext ctx) {
        PXValue condition = this.visit(ctx.expression(0));
        if (condition.asBoolean()) {
            return new PXValue(this.visit(ctx.expression(1)));
        } else {
            return new PXValue(this.visit(ctx.expression(2)));
        }
    }

    @Override
    public PXValue visitInExpression(InExpressionContext ctx) {
        PXValue lhs = this.visit(ctx.expression(0));
        PXValue rhs = this.visit(ctx.expression(1));

        if (rhs.isList()) {
            for (PXValue val : rhs.asList()) {
                if (val.equals(lhs)) {
                    return new PXValue(true);
                }
            }
            return new PXValue(false);
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitNumberExpression(@NotNull ProgramXParser.NumberExpressionContext ctx) {
        return new PXValue(Double.valueOf(ctx.getText()));
    }

    @Override
    public PXValue visitBoolExpression(@NotNull ProgramXParser.BoolExpressionContext ctx) {
        return new PXValue(Boolean.valueOf(ctx.getText()));
    }

    @Override
    public PXValue visitNullExpression(@NotNull ProgramXParser.NullExpressionContext ctx) {
        return PXValue.NULL;
    }

    private PXValue resolveIndexes(ParserRuleContext ctx, PXValue val, List<ExpressionContext> indexes) {
        for (ExpressionContext ec : indexes) {
            PXValue idx = this.visit(ec);
            if (!idx.isNumber() || (!val.isList() && !val.isString())) {
                throw new EvalException("Problem resolving indexes on " + val + " at " + idx, ec);
            }
            int i = idx.asDouble().intValue();
            if (val.isString()) {
                val = new PXValue(val.asString().substring(i, i + 1));
            } else {
                val = val.asList().get(i);
            }
        }
        return val;
    }

    private void setAtIndex(ParserRuleContext ctx, List<ExpressionContext> indexes, PXValue val, PXValue newVal) {
        if (!val.isList()) {
            throw new EvalException(ctx);
        }
        // TODO some more list size checking in here
        for (int i = 0; i < indexes.size() - 1; i++) {
            PXValue idx = this.visit(indexes.get(i));
            if (!idx.isNumber()) {
                throw new EvalException(ctx);
            }
            val = val.asList().get(idx.asDouble().intValue());
        }
        PXValue idx = this.visit(indexes.get(indexes.size() - 1));
        if (!idx.isNumber()) {
            throw new EvalException(ctx);
        }
        val.asList().set(idx.asDouble().intValue(), newVal);
    }

    @Override
    public PXValue visitFunctionCallExpression(FunctionCallExpressionContext ctx) {
        PXValue val = this.visit(ctx.functionCall());
        if (ctx.indexes() != null) {
            List<ExpressionContext> exps = ctx.indexes().expression();
            val = resolveIndexes(ctx, val, exps);
        }
        return val;
    }

    @Override
    public PXValue visitListExpression(ListExpressionContext ctx) {
        PXValue val = this.visit(ctx.list());
        if (ctx.indexes() != null) {
            List<ExpressionContext> exps = ctx.indexes().expression();
            val = resolveIndexes(ctx, val, exps);
        }
        return val;
    }

    @Override
    public PXValue visitIdentifierExpression(@NotNull ProgramXParser.IdentifierExpressionContext ctx) {
        String id = ctx.Identifier().getText();
        PXValue val = scope.resolve(id);

        if (ctx.indexes() != null) {
            List<ExpressionContext> exps = ctx.indexes().expression();
            val = resolveIndexes(ctx, val, exps);
        }
        return val;
    }

    @Override
    public PXValue visitStringExpression(@NotNull ProgramXParser.StringExpressionContext ctx) {
        String text = ctx.getText();
        text = text.substring(1, text.length() - 1).replaceAll("\\\\(.)", "$1");
        PXValue val = new PXValue(text);
        if (ctx.indexes() != null) {
            List<ExpressionContext> exps = ctx.indexes().expression();
            val = resolveIndexes(ctx, val, exps);
        }
        return val;
    }

    @Override
    public PXValue visitExpressionExpression(ExpressionExpressionContext ctx) {
        PXValue val = this.visit(ctx.expression());
        if (ctx.indexes() != null) {
            List<ExpressionContext> exps = ctx.indexes().expression();
            val = resolveIndexes(ctx, val, exps);
        }
        return val;
    }

    @Override
    public PXValue visitInputExpression(InputExpressionContext ctx) {
        TerminalNode inputString = ctx.Line();
        try {
            if (inputString != null) {
                String text = inputString.getText();
                text = text.substring(1, text.length() - 1).replaceAll("\\\\(.)", "$1");
                return new PXValue(new String(Files.readAllBytes(Paths.get(text))));
            } else {
                BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
                return new PXValue(buffer.readLine());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PXValue visitAssignment(@NotNull ProgramXParser.AssignmentContext ctx) {
        PXValue newVal = this.visit(ctx.expression());
        if (ctx.indexes() != null) {
            PXValue val = scope.resolve(ctx.Identifier().getText());
            List<ExpressionContext> exps = ctx.indexes().expression();
            setAtIndex(ctx, exps, val, newVal);
        } else {
            String id = ctx.Identifier().getText();
            scope.assign(id, newVal);
        }
        return PXValue.VOID;
    }

    @Override
    public PXValue visitIdentifierFunctionCall(IdentifierFunctionCallContext ctx) {
        List<ExpressionContext> params = ctx.exprList() != null ? ctx.exprList().expression() : new ArrayList<ExpressionContext>();
        String id = ctx.Identifier().getText() + params.size();
        Function function;
        if ((function = functions.get(id)) != null) {
            return function.invoke(params, functions, scope);
        }
        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitPrintlnFunctionCall(@NotNull ProgramXParser.PrintlnFunctionCallContext ctx) {
        System.out.println(this.visit(ctx.expression()));
        return PXValue.VOID;
    }

    @Override
    public PXValue visitPrintFunctionCall(@NotNull ProgramXParser.PrintFunctionCallContext ctx) {
        System.out.print(this.visit(ctx.expression()));
        return PXValue.VOID;
    }
    
    @Override
    public PXValue visitLogFunctionCall(@NotNull ProgramXParser.LogFunctionCallContext ctx) {
        List<ExpressionContext> params = ctx.exprList() != null ? ctx.exprList().expression() : new ArrayList<ExpressionContext>();
        String path = "log.txt";
        PXValue expr;
        if (params.size() == 2) {
            path = this.visit(params.get(0)).toString();
            expr = this.visit(params.get(1));     
        } else {
            expr = this.visit(params.get(0));
        }
        try(  PrintWriter out = new PrintWriter( path )  ){
            out.println( expr );
            System.out.println("Spausdinta faile: " + path);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ASTWalker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return PXValue.VOID;
    }

    @Override
    public PXValue visitAssertFunctionCall(AssertFunctionCallContext ctx) {
        PXValue value = this.visit(ctx.expression());

        if (!value.isBoolean()) {
            throw new EvalException(ctx);
        }

        if (!value.asBoolean()) {   
            throw new AssertionError("Failed Assertion " + ctx.expression().getText() + " line:" + ctx.start.getLine());
        }

        return PXValue.VOID;
    }

    @Override
    public PXValue visitSizeFunctionCall(SizeFunctionCallContext ctx) {
        PXValue value = this.visit(ctx.expression());

        if (value.isString()) {
            return new PXValue(value.asString().length());
        }

        if (value.isList()) {
            return new PXValue(value.asList().size());
        }

        throw new EvalException(ctx);
    }

    @Override
    public PXValue visitIfStatement(@NotNull ProgramXParser.IfStatementContext ctx) {

        // if ...
        if (this.visit(ctx.ifStat().expression()).asBoolean()) {
            return this.visit(ctx.ifStat().block());
        }

        // else if ...
        for (int i = 0; i < ctx.elseIfStat().size(); i++) {
            if (this.visit(ctx.elseIfStat(i).expression()).asBoolean()) {
                return this.visit(ctx.elseIfStat(i).block());
            }
        }

        // else ...
        if (ctx.elseStat() != null) {
            return this.visit(ctx.elseStat().block());
        }

        return PXValue.VOID;
    }

    @Override
    public PXValue visitBlock(BlockContext ctx) {

        scope = new Scope(scope);
        for (StatementContext sx : ctx.statement()) {
            this.visit(sx);
        }
        ExpressionContext ex;
        if ((ex = ctx.expression()) != null) {
            returnValue.value = this.visit(ex);
            scope = scope.parent();
            throw returnValue;
        }
        scope = scope.parent();
        return PXValue.VOID;
    }

    @Override
    public PXValue visitForStatement(ForStatementContext ctx) {
        int start = this.visit(ctx.expression(0)).asDouble().intValue();
        int stop = this.visit(ctx.expression(1)).asDouble().intValue();
        for (int i = start; i <= stop; i++) {
            scope.assign(ctx.Identifier().getText(), new PXValue(i));
            PXValue returnValue = this.visit(ctx.block());
            if (returnValue != PXValue.VOID) {
                return returnValue;
            }
        }
        return PXValue.VOID;
    }

}
